#!/bin/sh
# Summary: This wrapper invokes the neighboring python script.
# Created: 2021-10-31 13:32:20
# Author:  Bryant Finney <bfinney@morsecorp.com> (https://bryant-finney.github.io/about)

pyscript="$(echo "$0" | rev | cut -d'.' -f2- | rev | xargs printf '%s.py')"
$pyscript "$@" && echo "exercise 3: success ✅"
