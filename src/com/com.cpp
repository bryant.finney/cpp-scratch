#include <com/com.h>
#include <math.h>
#include <stdio.h>

using namespace std;

namespace com {
    void goodMorning(const char* name) {
        printf("Good morning %s!\n", name);
    }

    void display_double_squares(int number, ll::LinkedList<dbl_sq_t>* list) {
        cout << number << ": ";
        list->reverse_recur();
        for (ll::Node<dbl_sq_t>* node = list->head; node != NULL;
            node = node->next) {

            cout << node->val.first << "^2 + " << node->val.second << "^2";
            if (node->next == NULL)
                cout << endl;
            else
                cout << " | ";
        }
    }

    int compute_double_squares(int number, ll::LinkedList<dbl_sq_t>* list) {
        for (int n_first = sqrt(number); n_first > 0; n_first--) {
            for (int n_second = 0;
                n_second <= n_first && pow(n_first, 2) + pow(n_second, 2) <= number;
                n_second++) {
                if (pow(n_first, 2) + pow(n_second, 2) == number) {
                    list->insert({ n_first, n_second });
                }
            }
        }
        return list->count();
    }

    void test_count(void) {
        ll::LinkedList<double> list;
        int count;
        for (double i = 1.0; i <= 10; i++) {
            count = list.insert(i);
            assert(count == i);
        }
        assert(list.count() == count);
        assert(list.count() == 10);
    }

    void test_reverse_iter(void) {
        ll::LinkedList<double> list;
        for (double i = 1.0; i <= 10; i++)
            list.insert(i);

        list.display();
        cout << "reversing..." << endl;
        list.reverse_iter();
        list.display();
    }

    void test_reverse_recur(void) {
        ll::LinkedList<double> list;
        for (double i = 1.0; i <= 10; i++)
            list.insert(i);

        list.display();
        cout << "reversing..." << endl;
        list.reverse_recur();
        list.display();
    }
}
