/**
 * @file    threading.cpp
 * @author  Bryant Finney (https://bryant-finney.github.io/about/)
 *
 * @brief   Provide the multithreading functionality for Exercise 3
 * @version 0.1
 * @date    2021-10-31
 */
#include <com/threading.h>
#include <assert.h>

using namespace std;

namespace threading {
    template <class T>
    bool Queue<T>::empty(void) {
        lock_guard<mutex> lock(this->_mutex);
        return queue<T>::empty();
    }

    template <class T>
    T& Queue<T>::back(void) {
        lock_guard<mutex> lock(this->_mutex);
        return queue<T>::back();
    }

    template <class T>
    T& Queue<T>::front(void) {
        lock_guard<mutex> lock(this->_mutex);
        return queue<T>::front();
    }

    template <class T>
    void Queue<T>::pop(void) {
        lock_guard<mutex> lock(this->_mutex);
        queue<T>::pop();
    }

    template <class T>
    T& Queue<T>::pop_front(void) {
        lock_guard<mutex> lock(this->_mutex);
        T& t = queue<T>::front();
        queue<T>::pop();
        return t;
    }


    template <class T>
    void Queue<T>::push(T& item) {
        lock_guard<mutex> lock(this->_mutex);
        return queue<T>::push(item);
    }


    template <class T>
    size_t Queue<T>::size(void) {
        lock_guard<mutex> lock(this->_mutex);
        return queue<T>::size();
    }

    void test_queue(void) {
        Queue<string> q;
        vector<string> v{ "this is a test string", "and another" };

        assert(q.empty());

        q.push(v[0]);

        assert(!q.empty());
        assert(q.size() == 1);

        q.push(v[1]);

        assert(q.size() == 2);

        assert(q.back().compare("this is a test string"));
        assert(q.front().compare("and another"));

        assert(q.pop_front().compare("and another"));
        assert(q.size() == 1);


    }

    template <class K, class V>
    V Map<K, V>::at(K k) {
        lock_guard<mutex> lock(this->_mutex);
        return map<K, V>::at(k);
    }

    template <class K, class V>
    void Map<K, V>::insert(pair<K, V> item) {
        lock_guard<mutex> lock(this->_mutex);
        map<K, V>::insert(item);
    }

    template <class K, class V>
    size_t Map<K, V>::size() {
        lock_guard<mutex> lock(this->_mutex);
        return map<K, V>::size();
    }


    void test_map(void) {
        map<int, int> m{ {5,5}, };
        m.insert({ 6, 6 });
        assert(m.size() == 2);
        assert(m.at(5) == 5);
    }

    void Pool::add_job(Job job) {
        this->jobs.push(job);
        this->cv.notify_one();
    }

    int Pool::get_result(char k) {
        return this->results.at(k);
    }

    void Pool::finish(void) {
        {
            unique_lock<mutex> lock(this->ctrl_mutex);
            this->cv.wait(lock, [&]() {return !this->stop_flag; });
            this->stop_flag = true;
        }
        this->cv.notify_all();
        for (int i_thread = 0; i_thread < this->workers.size(); i_thread++)
            this->workers[i_thread].join();
    }

    void do_work(
        Queue<Job>* jobs,
        Map<char, int>* results,
        bool* stop_flag,
        mutex* ctrl_mutex,
        condition_variable* cv
    ) {
        Job job;
        int res;

        while (true) {
            {
                unique_lock<mutex> lock(*ctrl_mutex);
                cv->wait(lock, [&]() {return (!jobs->empty()) || *stop_flag; });

                if (jobs->empty() && *stop_flag)
                    return;
                job = jobs->pop_front();
            }
            res = job.func(job.c, job.s);
            results->insert({ job.c, res });
        }
    }
} // namespace threading
