# Get all the C++ files composing the com library
file(GLOB com_FILES ${CMAKE_CURRENT_SOURCE_DIR}/com/*.cpp)

# Create a shared library using these files
add_library(com SHARED ${com_FILES})

# Define the include directory for the library
target_include_directories(com PUBLIC ${CMAKE_SOURCE_DIR}/include/scratch)

# Set the C++ standard to use for this library
target_compile_features(com PUBLIC cxx_std_11)

# Link with the fmt library
# target_link_libraries(com fmt::fmt)
