#include <com/com.h>
#include <iostream>
#include <stdexcept>

using namespace std;

int main(int argc, char* argv[]) {
    string arg; int val;
    ll::LinkedList<com::dbl_sq_t> list;

    if (argc < 2)
        throw invalid_argument("one or more arguments must be provided");

    cout << "# Beginning exercise 2" << endl << endl;
    for (int i_arg = 1; i_arg < argc; i_arg++) {
        list = ll::LinkedList<com::dbl_sq_t>();
        arg = string(argv[i_arg]);
        val = stoi(arg);

        com::compute_double_squares(val, &list);
        com::display_double_squares(val, &list);
    }

    cout << endl << "Complete!" << endl;
}
