/**
 * @file    threading.h
 * @author  Bryant Finney (https://bryant-finney.github.io/about/)
 *
 * @brief   Declare the functionality provided by `threading.cpp`
 * @version 0.1
 * @date    2021-10-31
 */
#ifndef THREADING_H
#define THREADING_H

#include <condition_variable>
#include <initializer_list>
#include <map>
#include <mutex>
#include <queue>
#include <thread>
#include <vector>

#define THIS_MUTEX(action) this._mutex.lock(); action; this._mutex.unlock()

using namespace std;

namespace threading {

    /**
     * @brief   Define the type for job pointers.
     *
     * @todo    Generalize this typedef to support other job signatures
     */
    typedef int (*func_t)(char c, string s);

    class Job {
    public:
        func_t func;
        char c;
        string s;
    };

    /**
     * @brief       Implement a threadsafe queue.
     *
     * @details     Each method implemented here simply calls the parent method within
     *                  a lock context
     *
     * @tparam T    store this object type in the queue
     */
    template<class T>
    class Queue : queue<T> {

        mutable mutex _mutex;

    public:
        Queue() : queue<T>() {};
        bool empty(void);
        T& back(void);
        T& front(void);
        void pop(void);
        T& pop_front(void);
        void push(T& item);
        size_t size(void);
    };

    template<class K, class V>
    class Map : map<K, V> {

        mutable mutex _mutex;

    public:
        using map<K, V>::map;
        V at(K);
        void insert(pair<K, V>);
        size_t size(void);
    };

    void do_work(Queue<Job>*, Map<char, int>*, bool*, mutex*, condition_variable*);

    class Pool {
        vector<thread> workers;
        bool stop_flag;
        mutable mutex ctrl_mutex;
        condition_variable cv;

        Pool(int n) {
            for (int i = 0; i < n; i++)
                workers.push_back(
                    thread(do_work, &jobs, &results, &stop_flag, &ctrl_mutex, &cv)
                );
        };
    public:
        Queue<Job> jobs;
        Map<char, int> results;

        void add_job(Job);
        void finish(void);
        int get_result(char k);
        Pool() : Pool(thread::hardware_concurrency()) {};

    };

    void test_queue(void);
    void test_map(void);

}  // namespace threading

#endif  // ndef THREADING_H
