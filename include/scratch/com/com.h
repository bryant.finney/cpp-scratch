/**
 * @file linked.cpp
 * @author Bryant Finney (https://bryant-finney.github.io/about)
 * @brief Define common classes and functions for the exercise apps.
 * @version 0.1
 * @date 2021-10-30
 */
#ifndef COM_COM_H
#define COM_COM_H

#define NODE_TYPE_DEFAULT int

#pragma once

#include <cassert>
#include <iostream>
#include <stdexcept>
#include <stdio.h>
#include <string>

 /**
  * @brief Declare the namespace for exercise 1
  */
namespace ll {
    /**
     * @brief Model a single-linked list.
     *
     * @tparam T
     */
    template <class T = NODE_TYPE_DEFAULT>
    class Node {
    public:
        T val;
        Node<T>* next;
        Node() {};
        Node(T value) { val = value; next = NULL; };
        Node(T value, Node<T> node) { val = value; next = node; };
    };

    template <class T = NODE_TYPE_DEFAULT>
    class LinkedList {
    public:
        Node<T>* head;

        LinkedList(void) { head = NULL; _count = 0; };
        int count(void) { return _count; };

        /**
         * @brief           Write the list to `stdout`.
         * @tparam T        the type of data stored in the list
         */
        void display(void) {
            std::cout << _count << " items: ";
            for (Node<T>* node = head; node != NULL; node = node->next) {
                std::cout << node->val;
                if (node->next != NULL) std::cout << " ";
            }
            std::cout << std::endl;
        };

        /**
         * @brief           Create a new node in the list from the provided value.
         * @param val       the node's value
         * @tparam T        the type of data stored in the list
         * @return int      the number of items in the list
         */
        int insert(T val) {
            Node<T>* new_node = new Node<T>(val);

            if (head == NULL)
                head = new_node;
            else {
                new_node->next = head;
                head = new_node;
            }
            return ++_count;
        };

        /**
         * @brief                   Remove the given value from the list.
         * @param val               the value of the node to remove
         * @tparam T                the type of data stored in the list
         * @throws domain_error     the given value is not present in the list
         * @return int              the number of items in the list
         */
        int remove(T val) {
            if (head->val == val) {
                delete head;
                head = head->next;
                return --_count;
            }

            if (head->next == NULL) {
                if (head->val == val) {
                    delete head;
                    head = NULL;
                    return --_count;
                }
                throw std::domain_error("the given value is not in this list");
            }

            for (Node<T>* node = head->next; node->next != NULL; ) {
                // When node is found, delete the node and modify the pointers
                if (node->next->val == val) {
                    Node<T>* temp_ptr = node->next->next;
                    delete node->next;
                    node->next = temp_ptr;
                    return --_count;
                }
                node = node->next;
            }
            throw std::domain_error("the given value is not in this list");
        };

        void reverse_iter(void) {
            if (head == NULL)
                throw std::domain_error("no items in list");

            Node<T>* last, * next;

            for (last = NULL, next = head->next, head->next = last; next != NULL; head->next = last) {
                last = head;
                head = next;
                next = next->next;
            }
        };

        void reverse_recur(void) {
            head = reverse_recur(head, NULL);
        };

        /**
         * @brief                   Search the list for the given value.
         * @param val               the value of the node to search for
         * @tparam T                the type of data stored in the list
         * @return true
         * @return false
         */
        bool search(T val) {
            for (Node<T>* node = head; node != NULL; node = node->next) {
                if (node->val == val)
                    return true;
            }
            return false;
        };

    private:
        int _count = 0;
        Node<T>* reverse_recur(Node<T>* node, Node<T>* last) {
            Node<T>* next = node->next;

            node->next = last;
            if (next != NULL)
                node = reverse_recur(next, node);

            return node;
        };
    };

}


namespace com {

    /**
     * @brief Model an integer as the sum of two squares.
     */
    typedef struct dbl_sq {
        int first; int second;
    } dbl_sq_t;

    /**
     * @brief Display the linked list of double squares.
     *
     * @param list      display the values in the given linked list
     */
    void display_double_squares(int number, ll::LinkedList<com::dbl_sq_t>* list);

    /**
     * @brief Get the double squares of the given number.
     *
     * @note The current implementation uses an inefficient brute-force approach with
     *          algorithmic efficiency of O(N)
     *
     * @param number    compute the double squares of this number (if they exist)
     * @param list      add the `dbl_sq_t` values to this linked list
     *
     * @return int      the number of double squares found for the given number
     */
    int compute_double_squares(int number, ll::LinkedList<dbl_sq_t>* list);

    /**
     * Say good morning to someone
     * @param name the name of the person
     */
    void goodMorning(const char* name);

    /**
     * @brief Test the `count()` method of the linked list (exercise 1).
     */
    void test_count(void);

    /**
     * @brief Test the reverse_iter() method.
     */
    void test_reverse_iter(void);

    /**
     * @brief Test the reverse_recur() method.
     */
    void test_reverse_recur(void);
}


#endif
