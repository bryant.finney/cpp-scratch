cmake_minimum_required(VERSION 3.2)

# Project description
project(
    cpp-scratch
    VERSION 1.0
    DESCRIPTION "A CMake/C++ practice project"
    LANGUAGES CXX)

# Process the src and apps directories
add_subdirectory(src)
add_subdirectory(apps)

# Force the generation of a compile_commands.json file to provide autocompletion for IDEs
set(CMAKE_EXPORT_COMPILE_COMMANDS TRUE CACHE BOOL "" FORCE)
set(CMAKE_BUILD_TYPE Debug)
set(THREADS_PREFER_PTHREAD_FLAG ON)
find_package(Threads REQUIRED)
