# Practice Exercises

[![Open in Visual Studio Code](https://open.vscode.dev/badges/open-in-vscode.svg)](vscode://ms-vscode-remote.remote-containers/cloneInVolume?url=https://gitlab.com/bryant.finney/cpp-scratch.git)

This project implements three exercises for C/C++ practice.

## Exercise 1

Implement a single-linked list template class.

The template class should provide two methods, each of which reverses the list order.
The first method uses recursion to perform the operation, and the second uses iteration.
Each method uses the list directly; no copies are generated.

### Designs

![flow](apps/exercise_1/flow.drawio.svg)

## Exercise 2

A double-square number is an integer that can be expressed as the sum of two perfect
squares. Implement an executable that accepts a single parameter `N` and returns the
permutations of double squares.

## Exercise 3

Given an array of `uchar`s, implement a multi-threaded application that computes the
frequency of which each `uchar` occurs.

### Designs

![threading](apps/exercise_3/design.drawio.svg)
